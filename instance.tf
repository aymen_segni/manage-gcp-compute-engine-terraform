#Variables declaration
variable "instance_name" {
  type = "string"
}
variable "disk_name" {
  type = "string"
}
variable "zone" {
  type = "string"
}
variable "size" {
}
# Disks

resource "google_compute_disk" "blog" {  
    name  = "${var.disk_name}"
    type  = "pd-ssd"
    zone  = "${var.zone}"
    size = "${var.size}"
}

# VM Instances

resource "google_compute_instance" "blog" {  
    name = "${var.instance_name}"
    machine_type = "f1-micro"
    zone = "${var.zone}"

    tags = ["http"]

    boot_disk {
        source = "${google_compute_disk.blog.name}"
        auto_delete = true
    }
    network_interface {
        network = "default"
        access_config {
            # ephemeral external ip address
        }
    }

    scheduling {
        preemptible = false
        on_host_maintenance = "MIGRATE"
        automatic_restart = true
    }

    provisioner "remote-exec" {
        inline = [
            "echo -e 'hello from $HOST' > ~/terraform_complete"
        ]
    
    }

}
