# Firewall Rules
resource "google_compute_firewall" "allow-ssh-office" {  
    name = "allow-ssh-office"
    network = "default"

    allow {
        protocol = "tcp"
        ports = ["22"]
    }

}

resource "google_compute_firewall" "allow-http" {  
    name = "allow-http"
    network = "default"

    allow {
        protocol = "tcp"
        ports = ["80"]
    }

    source_ranges = ["0.0.0.0/0"]
    target_tags = ["http"]
}
